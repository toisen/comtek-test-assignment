import spock.lang.Specification

class BlockingQueueTest extends Specification {

    class StringBlockingQueue extends BlockingQueue<String> {
        StringBlockingQueue(int size) {
            super(size)
        }
    }

    def "Main functionality"() {

        given:
        def stringBlockingQueue = new StringBlockingQueue(10)
        def output = new ArrayList<String>()

        when:
        for (def i = 0; i < 10; i++) {
            stringBlockingQueue.push(i.toString())
        }

        then:
        for (def i = 0; i < 10; i++) {
            output.add(stringBlockingQueue.pop())
        }
        output.size() == 10
    }
}

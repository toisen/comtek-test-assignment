import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class BlockingQueue<T> {
    private volatile List<T> body = Collections.synchronizedList(new LinkedList<>());
    private volatile int sizeLimit;

    BlockingQueue(int sizeLimit) {
        this.sizeLimit = sizeLimit;
    }

    protected synchronized void push(T item) throws InterruptedException {
        while (body.size() == this.sizeLimit) {
            wait();
        }
        if (body.size() == 0) {
            notifyAll();
        }
        body.add(item);
    }

    protected synchronized T pop() throws InterruptedException {
        while (body.size() == 0) {
            wait();
        }
        if (body.size() == sizeLimit) {
            notifyAll();
        }
        return body.remove(0);
    }
}
